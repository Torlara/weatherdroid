package com.example.asus.weatherdroid.API.request;

import com.example.asus.weatherdroid.API.response.ForecastResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public interface ForecastRequest {
    @GET("/weather.ashx")
    void getForecastResponse(@Query("q") String city,
                             @Query("format") String format,
                             @Query("num_of_days") int num_of_days,
                             @Query("fx") String fx,
                             @Query("cc") String cc,
                             @Query("fx24") String fx24,
                             @Query("tp") String tp,
                             @Query("key") String key,
                             Callback<ForecastResponse> cb);
}
