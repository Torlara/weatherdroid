package com.example.asus.weatherdroid.API;

import android.util.Log;

import com.example.asus.weatherdroid.API.request.DetailsRequest;
import com.example.asus.weatherdroid.API.request.ForecastRequest;
import com.example.asus.weatherdroid.API.response.DetailsResponse;
import com.example.asus.weatherdroid.API.response.ErrorResponse;
import com.example.asus.weatherdroid.API.response.ForecastResponse;
import com.example.asus.weatherdroid.WeatherConfig;
import com.example.asus.weatherdroid.model.DetailsModel;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public class APICall {

    public void getForecast(String city, String apiKey, int numbOfDays) {
        RestAdapter retrofit = new RestAdapter.Builder()
                //.setLogLevel(RestAdapter.LogLevel.FULL) // only for debug
                .setEndpoint(WeatherConfig.API_URL)
                .build();

        ForecastRequest mForecastRequest = retrofit.create(ForecastRequest.class);

        Callback<ForecastResponse> callback = new Callback<ForecastResponse>() {

            @Override
            public void success(ForecastResponse forecastWeatherResponse, Response response) {
                if (forecastWeatherResponse.getData().getWeather() == null) {
                    EventBus.getDefault().postSticky(new ErrorResponse("No such location"));
                } else {
                    if (forecastWeatherResponse.getData().getWeather().size() == WeatherConfig.WEATHER_FORECAST_DAYS) {
                        //Log.w("weather", forecastWeatherResponse.getData().weather+"");

                        //Log.w("model", forecastWeatherResponse.getData().getWeather().get(0).getHourly().get(0).getTempC()+"");
                        EventBus.getDefault().postSticky(forecastWeatherResponse.getData().getWeather());
                    } else {
                        String error = "";
                        if (forecastWeatherResponse.getData() != null &&
                                forecastWeatherResponse.getData().getError() != null &&
                                forecastWeatherResponse.getData().getError().size() > 0) {
                            error = forecastWeatherResponse.getData().getError().get(0).getMsg();
                        } else if (forecastWeatherResponse.getResults() != null &&
                                forecastWeatherResponse.getResults().getError() != null) {
                            error = forecastWeatherResponse.getResults().getError().getType();
                            error = error + ": " + forecastWeatherResponse.getResults().getError().getMessage();
                        }
                        EventBus.getDefault().postSticky(new ErrorResponse(error));
                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                EventBus.getDefault().postSticky(new ErrorResponse(error.getMessage()));
            }
        };

        mForecastRequest.getForecastResponse(city, "json", numbOfDays, "yes", "yes", "no", "24", apiKey, callback);

    }

    public void getDetailsForDay(String city, String weatherDate, String apiKey) {
        RestAdapter retrofit = new RestAdapter.Builder()
                //.setLogLevel(RestAdapter.LogLevel.FULL) // only for debug
                .setEndpoint(WeatherConfig.API_URL)
                .build();

        DetailsRequest mDetailsRequest = retrofit.create(DetailsRequest.class);
        Callback<DetailsResponse> callback = new Callback<DetailsResponse>() {

            @Override
            public void success(DetailsResponse todayWeatherResponse, Response response) {
                //Log.w("model", todayWeatherResponse.getData().getWeather().get(0).getHourly().get(0).getTempC()+"");

                if (todayWeatherResponse.getData() != null &&
                        todayWeatherResponse.getData().getWeather().size() > 0 &&
                        todayWeatherResponse.getData().getWeather().get(0) != null) {
                    //Log.w("details", todayWeatherResponse.getData()+"");
                    EventBus.getDefault().postSticky(todayWeatherResponse.getData().getWeather().get(0));
                } else {// manage error
                    String error = "";
                    if (todayWeatherResponse.getData() != null &&
                            todayWeatherResponse.getData().getError() != null &&
                            todayWeatherResponse.getData().getError().size() > 0) {
                        error = todayWeatherResponse.getData().getError().get(0).getMsg();
                    } else if (todayWeatherResponse.getResults() != null &&
                            todayWeatherResponse.getResults().getError() != null) {
                        error = todayWeatherResponse.getResults().getError().getType();
                        error = error + ": " + todayWeatherResponse.getResults().getError().getMessage();
                    }
                    EventBus.getDefault().postSticky(new ErrorResponse(error));
                }

            }

            @Override
            public void failure(RetrofitError error) {
                EventBus.getDefault().postSticky(new ErrorResponse(error.getMessage()));
            }
        };
        Log.w("data", weatherDate + "");
        mDetailsRequest.getDetailsResponse(city, "json", 1, weatherDate, "yes", "no", "24", apiKey, callback);

    }

}
