package com.example.asus.weatherdroid.API.request;

import com.example.asus.weatherdroid.API.response.SearchResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Torlara on 2015-10-21.
 * Yay!
 */
public interface SearchRequest {
    @GET("/search.ashx")
    void getDetailsResponse(@Query("query") String query,
                            @Query("format") String format,
                            @Query("key") String key,
                            Callback<SearchResponse> cb);
}
