package com.example.asus.weatherdroid.API.response;


import com.example.asus.weatherdroid.model.SearchModel;

import java.util.ArrayList;

/**
 * Created by Torlara on 2015-10-21.
 * Yay!
 */
public class SearchResponse {
    public Data data;
    public Results results;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Results getResults() {
        return results;
    }

    public void setResults(Results results) {
        this.results = results;
    }

    public class Data {
        public ArrayList<SearchModel> weather;
        public ArrayList<Error> error;

        public ArrayList<SearchModel> getWeather() {
            return weather;
        }

        public void setWeather(ArrayList<SearchModel> weather) {
            this.weather = weather;
        }

        public ArrayList<Error> getError() {
            return error;
        }

        public void setError(ArrayList<Error> error) {
            this.error = error;
        }

        public class Error {
            public String msg;

            public String getMsg() {
                return msg;
            }

            public void setMsg(String msg) {
                this.msg = msg;
            }
        }
    }

    public class Results {
        public Error error;

        public class Error {
            public String type;
            public String message;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }
        }

        public Error getError() {
            return error;
        }

        public void setError(Error error) {
            this.error = error;
        }
    }

}
