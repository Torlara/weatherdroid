package com.example.asus.weatherdroid.event;

/**
 * Created by Torlara on 2015-10-19.
 * Yay!
 */
public class LocationEvent {
    public final String location;

    public LocationEvent(String location) {
        this.location = location;
    }
}
