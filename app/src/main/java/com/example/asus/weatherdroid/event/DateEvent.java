package com.example.asus.weatherdroid.event;

/**
 * Created by Torlara on 2015-10-19.
 * Yay!
 */
public class DateEvent {
    public final String date;

    public DateEvent(String date) {
        this.date = date;
    }
}
