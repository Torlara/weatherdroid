package com.example.asus.weatherdroid.fragment;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.weatherdroid.API.APICall;
import com.example.asus.weatherdroid.API.response.ErrorResponse;
import com.example.asus.weatherdroid.LoadImageUtil;
import com.example.asus.weatherdroid.event.LocationEvent;
import com.example.asus.weatherdroid.event.NetworkChange;
import com.example.asus.weatherdroid.R;
import com.example.asus.weatherdroid.WeatherConfig;
import com.example.asus.weatherdroid.adapter.ForecastAdapter;
import com.example.asus.weatherdroid.model.ForecastModel;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by Torlara on 2015-10-20.
 * Yay!
 */
public class ForecastFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String FORECAST_FRAGMENT_TAG = "FORECAST";

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView mLocation;
    private TextView mUpdate;
    private APICall mApiCall;
    private ArrayList<ForecastModel> mArrayList;
    private ForecastAdapter mForecastAdapter;
    private LoadImageUtil loadImageUtil;
    private SearchView searchView;
    private String city;

    //private static final String[] COUNTRIES = new String[] { "Belgium",
    //        "France", "France_", "Italy", "Germany", "Spain" };

    public static ForecastFragment newInstance() {
        ForecastFragment fragment = new ForecastFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ForecastFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        loadImageUtil = new LoadImageUtil(getActivity());
        if (savedInstanceState == null) {
            city = "Wroclaw";
        } else {
            city = savedInstanceState.getString("location");
        }

        LocationEvent location = new LocationEvent(city);
        EventBus.getDefault().postSticky(location);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_forecast, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycle_view_forecast);
        mUpdate = (TextView) rootView.findViewById(R.id.fragment_forecast_update);
        if (savedInstanceState != null) {
            // Restore last state
            mUpdate.setText(savedInstanceState.getString("update"));
        } else {
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            mUpdate.setText(today.format("%k:%M:%S"));
        }

        mLocation = (TextView) rootView.findViewById(R.id.fragment_forecast_location);

        if (mArrayList == null) mArrayList = new ArrayList<>();
        if (mForecastAdapter == null)
            mForecastAdapter = new ForecastAdapter(mArrayList, loadImageUtil);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mForecastAdapter);
        mRecyclerView.getItemAnimator().setChangeDuration(500);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_forecast_swipe_refresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.primary);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }

        if (!mSwipeRefreshLayout.isRefreshing() && mArrayList.size() == 0) { //If mSwipeRefreshLayout is not refreshing than we should call the API to receive data
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        requestDataFromServer(city);
                    }
                }
            }, 600);
        }

    }

    @Override
    public void onRefresh() {
        requestDataFromServer(city);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        //TODO make autocomplete in searchView to work
        /*
        String[] columnNames = {"_id","text"};
        final MatrixCursor cursor = new MatrixCursor(columnNames);
        String[] array = COUNTRIES; //if strings are in resources
        String[] temp = new String[2];
        int id = 0;
        for(String item : array){
            temp[0] = Integer.toString(id++);
            temp[1] = item;
            cursor.addRow(temp);
            Log.w("row", item + "");
        }
        final String[] from = {"text"};
        final int[] to = {R.id.autocompleteTx_search};
        */
        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        }
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be
                //searchView.setSuggestionsAdapter(new SimpleCursorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item, cursor, from, to));
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                //Here u can get the value "query" which is entered in the search box.
                Log.w("query", query + "");
                LocationEvent location = new LocationEvent(query);
                EventBus.getDefault().postSticky(location);
                searchView.clearFocus();
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        super.onCreateOptionsMenu(menu, menuInflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.action_search || super.onOptionsItemSelected(item);
    }

    public void onEvent(LocationEvent event) {
        requestDataFromServer(event.location);
        WeatherConfig.location = event.location;
        city = event.location;
        mLocation.setText(city);
    }

    public void onEvent(ArrayList<ForecastModel> arrayList) {
        mSwipeRefreshLayout.setRefreshing(false);
        mLocation.setVisibility(View.VISIBLE);
        mUpdate.setVisibility(View.VISIBLE);
        mForecastAdapter.setOffline(false);

        mArrayList.clear();
        mArrayList.addAll(arrayList);
        mForecastAdapter.notifyDataSetChanged();
    }

    public void onEvent(ErrorResponse errorResponse) {
        mSwipeRefreshLayout.setRefreshing(false);
        mLocation.setVisibility(View.GONE);
        mUpdate.setVisibility(View.GONE);
        mForecastAdapter.setOffline(true);

        //Log.w("mam blad", errorResponse.errorResponse+"");
    }

    private void requestDataFromServer(String city) {
        if (mApiCall == null) mApiCall = new APICall();
        mApiCall.getForecast(city, WeatherConfig.API_KEY, WeatherConfig.WEATHER_FORECAST_DAYS);
    }

    public void onEvent(NetworkChange networkChange) {
        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("location", mLocation.getText() + "");
        outState.putString("update", mUpdate.getText() + "");
    }
}
