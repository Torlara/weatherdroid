package com.example.asus.weatherdroid.model;

import java.util.ArrayList;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public class DetailsModel {

    public String date;
    public ArrayList<HourlyModel> hourly;
    public ArrayList<Astronomy> astronomy;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<HourlyModel> getHourly() {
        return hourly;
    }

    public void setHourly(ArrayList<HourlyModel> hourly) {
        this.hourly = hourly;
    }

    public ArrayList<Astronomy> getAstronomy() {
        return astronomy;
    }

    public void setAstronomy(ArrayList<Astronomy> astronomy) {
        this.astronomy = astronomy;
    }

    public class Astronomy {
        public String sunrise;

        public String getSunrise() {
            return sunrise;
        }

        public void setSunrise(String sunrise) {
            this.sunrise = sunrise;
        }
    }

    public class HourlyModel {
        public String tempC;
        public String humidity;
        public String pressure;
        public String precipMM;
        public String windspeedKmph;
        public String winddir16Point;
        public String chanceofrain;
        public String visibility;
        public String FeelsLikeC;

        public ArrayList<WeatherDesc> weatherDesc;
        public ArrayList<WeatherIconUrl> weatherIconUrl;

        public ArrayList<WeatherDesc> getWeatherDesc() {
            return weatherDesc;
        }

        public void setWeatherDesc(ArrayList<WeatherDesc> weatherDesc) {
            this.weatherDesc = weatherDesc;
        }

        public ArrayList<WeatherIconUrl> getWeatherIconUrl() {
            return weatherIconUrl;
        }

        public void setWeatherIconUrl(ArrayList<WeatherIconUrl> weatherIconUrl) {
            this.weatherIconUrl = weatherIconUrl;
        }

        public String getTempC() {
            return tempC;
        }

        public void setTempC(String tempC) {
            this.tempC = tempC;
        }

        public String getHumidity() {
            return humidity;
        }

        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }

        public String getPressure() {
            return pressure;
        }

        public void setPressure(String pressure) {
            this.pressure = pressure;
        }

        public String getPrecipMM() {
            return precipMM;
        }

        public void setPrecipMM(String precipMM) {
            this.precipMM = precipMM;
        }

        public String getWinddir16Point() {
            return winddir16Point;
        }

        public void setWinddir16Point(String winddir16Point) {
            this.winddir16Point = winddir16Point;
        }

        public String getWindspeedKmph() {
            return windspeedKmph;
        }

        public void setWindspeedKmph(String windspeedKmph) {
            this.windspeedKmph = windspeedKmph;
        }

        public String getFeelsLikeC() {
            return FeelsLikeC;
        }

        public void setFeelsLikeC(String FeelsLikeC) {
            this.FeelsLikeC = FeelsLikeC;
        }

        public String getVisibility() {
            return visibility;
        }

        public void setVisibility(String visibility) {
            this.visibility = visibility;
        }

        public String getChanceofrain() {
            return chanceofrain;
        }

        public void setChanceofrain(String chanceofrain) {
            this.chanceofrain = chanceofrain;
        }

        public class WeatherDesc {
            public String value;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public class WeatherIconUrl {
            public String value;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}