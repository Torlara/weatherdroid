package com.example.asus.weatherdroid.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.asus.weatherdroid.event.LocationEvent;
import com.example.asus.weatherdroid.event.NetworkChange;
import com.example.asus.weatherdroid.R;
import com.example.asus.weatherdroid.fragment.DetailsFragment;
import com.example.asus.weatherdroid.fragment.ForecastFragment;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_content, ForecastFragment.newInstance(), ForecastFragment.FORECAST_FRAGMENT_TAG);
            fragmentTransaction.commit();
        } else {
            ForecastFragment fragment = (ForecastFragment) getFragmentManager().findFragmentByTag(ForecastFragment.FORECAST_FRAGMENT_TAG);
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().findFragmentByTag(DetailsFragment.DETAILS_FRAGMENT_TAG) != null) {

            getFragmentManager().popBackStack(DetailsFragment.DETAILS_FRAGMENT_TAG,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }


    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!isNetworkAvailable(this)) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_SHORT).show();
        }

    }

    public void onEvent(NetworkChange networkChange) {
        if (!isNetworkAvailable(this)) {
            Toast.makeText(this, "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
