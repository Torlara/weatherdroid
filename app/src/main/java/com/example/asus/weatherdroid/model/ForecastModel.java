package com.example.asus.weatherdroid.model;

import java.util.ArrayList;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public class ForecastModel {
    public String date;
    public ArrayList<HourlyModel> hourly;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<HourlyModel> getHourly() {
        return hourly;
    }

    public void setHourly(ArrayList<HourlyModel> hourly) {
        this.hourly = hourly;
    }

    public class HourlyModel {
        public String tempC;
        public ArrayList<WeatherDesc> weatherDesc;
        public ArrayList<WeatherIconUrl> weatherIconUrl;

        public ArrayList<WeatherDesc> getWeatherDesc() {
            return weatherDesc;
        }

        public void setWeatherDesc(ArrayList<WeatherDesc> weatherDesc) {
            this.weatherDesc = weatherDesc;
        }

        public ArrayList<WeatherIconUrl> getWeatherIconUrl() {
            return weatherIconUrl;
        }

        public void setWeatherIconUrl(ArrayList<WeatherIconUrl> weatherIconUrl) {
            this.weatherIconUrl = weatherIconUrl;
        }

        public String getTempC() {
            return tempC;
        }

        public void setTempC(String tempC) {
            this.tempC = tempC;
        }

        public class WeatherDesc {
            public String value;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public class WeatherIconUrl {
            public String value;

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
