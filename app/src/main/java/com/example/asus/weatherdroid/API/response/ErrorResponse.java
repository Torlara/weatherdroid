package com.example.asus.weatherdroid.API.response;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public class ErrorResponse {

    public String errorResponse;

    public ErrorResponse(String errorResponse) {
        this.errorResponse = errorResponse;
    }

    public String getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(String errorResponse) {
        this.errorResponse = errorResponse;
    }
}
