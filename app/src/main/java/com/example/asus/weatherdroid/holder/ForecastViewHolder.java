package com.example.asus.weatherdroid.holder;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.weatherdroid.event.DateEvent;
import com.example.asus.weatherdroid.R;
import com.example.asus.weatherdroid.fragment.DetailsFragment;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.event.EventBus;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public class ForecastViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView forecastItemIcon;
    public TextView forecastItemDay;
    public TextView forecastItemTemp;
    public TextView forecastItemCondition;
    public CardView cardView;
    public Context context;

    public ForecastViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        forecastItemIcon = (ImageView) itemView.findViewById(R.id.fragment_forecast_weather_image);
        forecastItemDay = (TextView) itemView.findViewById(R.id.fragment_forecast_item_day);
        forecastItemTemp = (TextView) itemView.findViewById(R.id.fragment_forecast_item_temp);
        forecastItemCondition = (TextView) itemView.findViewById(R.id.fragment_forecast_item_weather_condition);
        cardView = (CardView) itemView.findViewById(R.id.card_view);
        cardView.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Date dt1 = (Date) view.getTag();

        DateEvent date = new DateEvent(new SimpleDateFormat("yyyy-MM-dd").format(dt1));
        EventBus.getDefault().postSticky(date);

        FragmentTransaction fragmentTransaction = ((Activity) context).getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.animator.gla_there_come, R.animator.gla_there_gone, R.animator.gla_back_gone, R.animator.gla_back_come);
        fragmentTransaction.replace(R.id.container_content, DetailsFragment.newInstance(), DetailsFragment.DETAILS_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(DetailsFragment.DETAILS_FRAGMENT_TAG);
        fragmentTransaction.commit();
    }

}
