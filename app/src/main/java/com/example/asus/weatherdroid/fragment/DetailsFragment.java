package com.example.asus.weatherdroid.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.weatherdroid.API.APICall;
import com.example.asus.weatherdroid.API.response.ErrorResponse;
import com.example.asus.weatherdroid.event.DateEvent;
import com.example.asus.weatherdroid.LoadImageUtil;
import com.example.asus.weatherdroid.event.NetworkChange;
import com.example.asus.weatherdroid.R;
import com.example.asus.weatherdroid.WeatherConfig;
import com.example.asus.weatherdroid.model.DetailsModel;

import de.greenrobot.event.EventBus;

public class DetailsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public static String DETAILS_FRAGMENT_TAG = "DETAILS";

    public LoadImageUtil loadImageUtil;

    private RelativeLayout mContainerLayout;
    private LinearLayout mErrorLayout;
    private ImageView mWeatherImage;
    private TextView mDateText;
    private TextView mWeatherConditionText;
    private TextView mHumidityText;
    private TextView mPrecipitationText;
    private TextView mPressureText;
    private TextView mWindSpeedText;
    private TextView mDirectionText;
    private TextView mChanceOfRainText;
    private TextView mFeelsLikeText;
    private TextView mSunriseText;
    private TextView mVisibilityText;
    private TextView mErrorSubtitle;
    private APICall mApiCall;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String mDate;

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadImageUtil = new LoadImageUtil(getActivity());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        mContainerLayout = (RelativeLayout) rootView.findViewById(R.id.fragment_today_container);
        mErrorLayout = (LinearLayout) rootView.findViewById(R.id.fragment_today_error_view);

        mErrorSubtitle = (TextView) rootView.findViewById(R.id.error_subtitle);

        mDateText = (TextView) rootView.findViewById(R.id.fragment_today_date);
        mWeatherConditionText = (TextView) rootView.findViewById(R.id.fragment_today_weather_condition);

        mWeatherImage = (ImageView) rootView.findViewById(R.id.fragment_today_weather_image);
        mHumidityText = (TextView) rootView.findViewById(R.id.fragment_today_humidity);
        mPrecipitationText = (TextView) rootView.findViewById(R.id.fragment_today_precipitation);
        mPressureText = (TextView) rootView.findViewById(R.id.fragment_today_pressure);
        mWindSpeedText = (TextView) rootView.findViewById(R.id.fragment_today_wind_speed);
        mDirectionText = (TextView) rootView.findViewById(R.id.fragment_today_direction);
        mChanceOfRainText = (TextView) rootView.findViewById(R.id.fragment_today_chance_of_rain);
        mSunriseText = (TextView) rootView.findViewById(R.id.fragment_today_sunrise);
        mFeelsLikeText = (TextView) rootView.findViewById(R.id.fragment_today_feels_like);
        mVisibilityText = (TextView) rootView.findViewById(R.id.fragment_today_visibility);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_today_swipe_refresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.primary);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mContainerLayout.setVisibility(View.GONE);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        mContainerLayout.setVisibility(View.GONE);
        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }

        if (mContainerLayout.getVisibility() == View.GONE && !mSwipeRefreshLayout.isRefreshing()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!mSwipeRefreshLayout.isRefreshing()) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        requestDataFromServer(WeatherConfig.location, mDateText.getText() + "");
                    }
                }
            }, 600);
        }

    }

    @Override
    public void onRefresh() {
        requestDataFromServer(WeatherConfig.location, mDate);
    }

    private void requestDataFromServer(String city, String date) {
        if (mApiCall == null) mApiCall = new APICall();
        mApiCall.getDetailsForDay(city, date, WeatherConfig.API_KEY);
    }

    private void loadData(DetailsModel detailsModel) {
        mDateText.setText(detailsModel.getDate());
        loadImageUtil.loadBitmapToImageView(mWeatherImage, detailsModel.getHourly().get(0).getWeatherIconUrl().get(0).getValue());
        mWeatherConditionText.setText(detailsModel.getHourly().get(0).getTempC() + "°" + " | " + detailsModel.getHourly().get(0).getWeatherDesc().get(0).getValue());
        mHumidityText.setText(detailsModel.getHourly().get(0).getHumidity() + "%");
        mPrecipitationText.setText(detailsModel.getHourly().get(0).getPrecipMM() + "mm");
        mPressureText.setText(detailsModel.getHourly().get(0).getPressure() + " hPa");
        mWindSpeedText.setText(detailsModel.getHourly().get(0).getWindspeedKmph() + " km/h");
        mDirectionText.setText(detailsModel.getHourly().get(0).getWinddir16Point());
        mChanceOfRainText.setText(detailsModel.getHourly().get(0).getChanceofrain() + "%");
        mSunriseText.setText(detailsModel.getAstronomy().get(0).getSunrise());
        mFeelsLikeText.setText(detailsModel.getHourly().get(0).getFeelsLikeC() + "°");
        mVisibilityText.setText(detailsModel.getHourly().get(0).getVisibility() + " km");

    }

    public void onEvent(DetailsModel detailsModel) {
        mSwipeRefreshLayout.setRefreshing(false);
        mContainerLayout.setVisibility(View.VISIBLE);
        mErrorLayout.setVisibility(View.GONE);
        loadData(detailsModel);
    }

    public void onEvent(DateEvent event) {
        mDate = event.date;
        requestDataFromServer(WeatherConfig.location, mDate);

    }

    public void onEvent(NetworkChange networkChange) {
        if (!isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_SHORT).show();
        }
    }

    public void onEvent(ErrorResponse errorResponse) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (errorResponse.getErrorResponse() != null && errorResponse.getErrorResponse().length() > 0)
            mErrorSubtitle.setText(errorResponse.getErrorResponse());
        mContainerLayout.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.VISIBLE);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
