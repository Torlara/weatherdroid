package com.example.asus.weatherdroid.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.asus.weatherdroid.LoadImageUtil;
import com.example.asus.weatherdroid.R;
import com.example.asus.weatherdroid.holder.ErrorViewHolder;
import com.example.asus.weatherdroid.holder.ForecastViewHolder;
import com.example.asus.weatherdroid.model.ForecastModel;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public class ForecastAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ERROR = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<ForecastModel> arrayList;
    private boolean isOffline;
    private Date date;
    private LoadImageUtil loadImageUtil;

    public ForecastAdapter(ArrayList<ForecastModel> arrayList, LoadImageUtil loadImageUtil) {
        this.arrayList = arrayList;
        this.loadImageUtil = loadImageUtil;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ERROR) {
            View rowViewE = LayoutInflater.from(parent.getContext()).inflate(R.layout.error_view, parent, false);
            return new ErrorViewHolder(rowViewE);

        } else if (viewType == TYPE_ITEM) {
            View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
            return new ForecastViewHolder(parent.getContext(), rowView);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ErrorViewHolder) {
            ErrorViewHolder holderError = (ErrorViewHolder) holder;
            if (isOffline) {
                holderError.linearLayout.setVisibility(View.VISIBLE);
            } else {
                holderError.linearLayout.setVisibility(View.GONE);
            }

        } else if (holder instanceof ForecastViewHolder) {
            ForecastViewHolder holderForecast = (ForecastViewHolder) holder;

            loadImageUtil.loadBitmapToImageView(holderForecast.forecastItemIcon, arrayList.get(position).getHourly().get(0).getWeatherIconUrl().get(0).getValue());
            holderForecast.forecastItemCondition.setText(arrayList.get(position).getHourly().get(0).getWeatherDesc().get(0).getValue());

            try {
                String input_date = arrayList.get(position).getDate();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                date = format1.parse(input_date);
                DateFormat format2 = new SimpleDateFormat("EEEE");
                holderForecast.forecastItemDay.setText(format2.format(date));

            } catch (ParseException e) {
                e.printStackTrace();
            }

            holderForecast.forecastItemTemp.setText(getTemperature(arrayList.get(position), holderForecast.forecastItemTemp.getContext()));
            holderForecast.cardView.setTag(date);
        }
    }

    @Override
    public int getItemCount() {
        if (arrayList.size() == 0 || isOffline)
            return 1;
        else
            return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (arrayList.size() == 0 || isOffline)
            return TYPE_ERROR;
        else
            return TYPE_ITEM;
    }

    public void setOffline(boolean isOffline) {
        this.isOffline = isOffline;
        if (isOffline)
            notifyDataSetChanged();
    }

    protected String getTemperature(ForecastModel forecastWeatherModel, Context mContext) {

        return forecastWeatherModel.getHourly().get(0).getTempC() + "°";
    }


}
