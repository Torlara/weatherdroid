package com.example.asus.weatherdroid.API.request;

import com.example.asus.weatherdroid.API.response.DetailsResponse;
import com.example.asus.weatherdroid.model.DetailsModel;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Torlara on 2015-10-18.
 * Yay!
 */
public interface DetailsRequest {
    @GET("/weather.ashx")
    void getDetailsResponse(@Query("q") String city,
                            @Query("format") String format,
                            @Query("num_of_days") int num_of_days,
                            @Query("date") String date,
                            @Query("fx") String fx,
                            @Query("cc") String cc,
                            @Query("tp") String tp,
                            @Query("key") String key,
                            Callback<DetailsResponse> cb);
}