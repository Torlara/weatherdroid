package com.example.asus.weatherdroid.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.asus.weatherdroid.R;

/**
 * Created by Torlara on 2015-10-19.
 * Yay!
 */
public class ErrorViewHolder extends RecyclerView.ViewHolder {

    public LinearLayout linearLayout;

    public ErrorViewHolder(View itemView) {
        super(itemView);
        linearLayout = (LinearLayout) itemView.findViewById(R.id.error_view_container);
    }

}
